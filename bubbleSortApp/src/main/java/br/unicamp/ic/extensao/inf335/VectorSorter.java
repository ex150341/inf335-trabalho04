package br.unicamp.ic.extensao.inf335;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VectorSorter {
	
	/**
	* Logger
	*/
	private static final Logger logger = LogManager.getLogger(VectorSorter.class);

	private static final int defaultVectorSize = 10;
	private static final int randomGenerationScale = 100;
	private static final int randomGenerationMinValue = 1;

	public static void main(String[] args) {
	
		int[] numbers = parseParameters(args);
	
		System.out.print("Input: ");
		logger.debug("Input: ");	
		printVector(numbers);
		sort(numbers);
		System.out.print("Sorted: ");
		printVector(numbers);
	}
	
	public static void sort(int[] numbers) {
		BubbleSort.sort(numbers);
	}
	
	public static int[] parseParameters(String[] args) {
		int[] numbers;
		if(args.length > 0) {
			numbers = new int[args.length];
			for(int k=0; k<args.length; k++) {
				numbers[k] = Integer.parseInt(args[k]);
			}
		}else {
			numbers = generateRandomVector(defaultVectorSize);
		}
		return numbers;
	}
	
	private static int[] generateRandomVector(int size) {

		int[] vector = new int[size];

		for (int i = 0; i < vector.length ; i++) {
			vector[i] = (int) (Math.random()*randomGenerationScale + randomGenerationMinValue);
		}
		
		return vector;
	}
	
	public static void printVector(int[] numbers) {
		System.out.print("[ ");
		logger.debug("[ ");

		System.out.print(numbers[0]);
		logger.debug(numbers[0]);

		int i = 0;
		do {
			i++;
			System.out.print(", ");
			logger.debug(", ");
			
			System.out.print(numbers[i]);
			logger.debug(numbers[i]);

		}while(i < numbers.length - 1);

		System.out.println(" ]");
		logger.debug(" ]");
	}
}
