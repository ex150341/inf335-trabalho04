package br.unicamp.ic.extensao.inf335;

/**
* <h1>Bubble Sort Class</h1>
* A class that implements bubble sort algorithm to inf335 class
*
* @author  Professor Leonardo Montecchi
* @version 1.0
* @since   2021-01-01
*/
public class BubbleSort {

/**
* This method is used to sort a vector of integers
* using the bubble sort algorithm
* @param vector The int vector that will be sorted.
*/
	public static void sort(int[] vector) {
		boolean switched = true;
		int aux;
		while (switched) {
			switched = false;
			for (int i = 0; i < vector.length - 1; i++) {
				if (vector[i] > vector[i + 1]) {
					switched = true;
					swap(vector, i);
				}
			}
		}
	}

/**
* This method is used to swap values of a given vector
* @param vector The int vector to swap values
* @param jump  The position that will be swapped
*/
	private static void swap(int[] vector, int jump) {
		int aux;
		aux = vector[jump];
		vector[jump] = vector[jump + 1];
		vector[jump+1] = aux; //Aqui 
	}
}

