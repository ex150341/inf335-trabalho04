package br.unicamp.ic.extensao.inf335;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class BubbleSortTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    /**
     * Testing the sorting method
    */
    @Test
    public void itShouldOrderAscending()
    {
 	int[] intArrayExpected = {1,2,3,4,5};
	int[] intArraySorted = {5,4,3,2,1};

        BubbleSort.sort(intArraySorted);

	assertArrayEquals(intArrayExpected,intArraySorted);
    }
}
